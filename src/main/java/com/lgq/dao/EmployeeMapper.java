package com.lgq.dao;


import com.lgq.bean.Employee;

public interface EmployeeMapper {

    Employee getEmpById(Integer id);

}
